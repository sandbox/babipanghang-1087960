<?php
/*
    Copyright 2011 Jacob Pieter van Wingerden

*/

include_once "renderengine_interface.php";
include_once '../helperfunctions.php';
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of renderengine_inkscape
 *
 * @author Jaap
 */
class renderengine_inkscape implements renderengine {
    private $svgdata;

    public function __construct() {
        $this->svgdata = false;
    }

    /*
     * Function getAvailableRenderFileFormats() should return an array containing
     * strings containing the extensions of all file formats that this render
     * engine can render to.
     *
     * NOTE: PDF is highly recommended.
     */
    public function getAvailableRenderFileFormats(){
        return array(
            'svg',
            'png',
            'pdf',
            'eps'
        );
    }

    /*
     * Function getImplementedFileFormats() should return an array containing
     * strings containing the file extensions of all file formats this engine accepts
     * as input file format.
     */
    public static function getImplementedFileFormats(){
        return array(
            'svg'
        );
    }

    /*
     * Function loadFile() accepts a string containing a filename of a file with data to be rendered.
     */
    public function loadFile($filename){
        $this->svgdata = file_get_contents($filename);
    }

    /*
     * Function loadString() accepts data to be rendered in the form of a string. Binary data
     * should be base64 encoded.
     */
    public function loadString($xmldata){
        if (!is_string($xmldata))
            throw new Exception('renderengine_inkscape: loadString() - Input data is not a valid string.');
        $this->svgdata = $xmldata;
    }

    /*
     * Function render() should output rendered data. Takes 3 arguments
     * - DPI: How many DPI should the output render be (when appliccable)
     * - fileformat: What format should the output be. Should be one of the
     *   results returned by getImplementedFileFormats() or null, which indicates
     *   a default output format should be chosen.
     * - filename: Specify the name of a file to output to. If not specified, the
     *   function should return a string containing the rendered result. If the
     *   output format in itself is an ASCII-file (like SVG), no encoding must
     *   be used. In case of binary formats (like PDF), use base64.
     */
    public function render($DPI = 300, $fileformat = null, $filename = null){
        if (is_bool($this->svgdata))
            throw new Exception('renderengine_inkscape: render() - Cannot render without data.');

        $outfile = tempnam('/tmp', 'renderer_inkscape_');
        if (!is_null($filename))
            $outfile = $filename;

        $switches = '';
        switch ($fileformat){
            case 'svg': {
                if (!is_null($filename)){
                    if (false === file_put_contents($filename, $this->svgdata))
                        throw new Exception('renderengine_inkscape: render() - Could not write to file '. $filename);
                    return $filename;
                }
                return $this->svgdata;
            }
            case 'png': {
                $switches = '--export-png="'. $filename. '" --export-dpi='. $DPI;
                break;
            }
            case 'eps': {
                $switches = '--export-eps="'. $filename. '"';
                break;
            }
            default: {
                $switches = '--export-pdf="'. $filename. '"';
                break;
            }
        }
        $inkscapeInputFile = tempnam('/tmp', 'renderer_inkscape_');
        shell_exec(variable_get('webtoprint_inkscapepath', find_inkscape_executable()). $switches. ' "'. $inkscapeInputFile. '"');
        return $filename;
    }
}

