<?php
/*
    Copyright 2011 Jacob Pieter van Wingerden
*/

/**
 *
 * @author Jaap
 */
interface renderengine {
    /*
     * Function getAvailableRenderFileFormats() should return an array containing
     * strings containing the extensions of all file formats that this render
     * engine can render to.
     *
     * NOTE: PDF is highly recommended.
     */
    public function getAvailableRenderFileFormats();


    /*
     * Function getImplementedFileFormats() should return an array containing
     * strings containing the file extensions of all file formats this engine accepts
     * as input file format.
     */
    public static function getImplementedFileFormats();

    /*
     * Function loadFile() accepts a string containing a filename of a file with data to be rendered.
     */
    public function loadFile($filename);

    /*
     * Function loadString() accepts data to be rendered in the form of a string. Binary data
     * should be base64 encoded.
     */
    public function loadString($xmldata);

    /*
     * Function render() should output rendered data. Takes 3 arguments
     * - DPI: How many DPI should the output render be (when appliccable)
     * - fileformat: What format should the output be. Should be one of the
     *   results returned by getImplementedFileFormats() or null, which indicates
     *   a default output format should be chosen.
     * - filename: Specify the name of a file to output to. If not specified, the
     *   function should return a string containing the rendered result. If the
     *   output format in itself is an ASCII-file (like SVG), no encoding must
     *   be used. In case of binary formats (like PDF), use base64.
     */
    public function render($DPI = 300, $fileformat = null, $filename = null);
}
?>
