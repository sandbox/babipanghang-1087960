<?php
/* 
 * Creates a replacement for the mime_content_type() function. At the time
 * this code was written, it was still functional, but depricated. This function
 * was created with future compatibility in mind. We'll trust on the original
 * mime_content_type function for as long as it's available.
 */
if (!function_exists('mime_content_type')){
    function mime_content_type($filename){
        $finfo = new finfo(FILEINFO_MIME); // return mime type ala mimetype extension

        if (!$finfo) {
            return 'data/unknown';
        }

        /* get mime-type for a specific file */
        return $finfo->file($filename);
    }
}


