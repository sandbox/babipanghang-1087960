<?php

foreach(glob('template_file_plugins/template_file_plugin_*.php') as $pluginFile){
    if (file_exists($pluginFile))
        include_once($pluginFile);
}


class template_engine {
    private $pluginlist;
    private $loadedPlugin = null;

    public function __construct() {
        $this->pluginlist = array();
        foreach(get_declared_classes() as $declaredclass){
            // a class that implements the template_plugin interface must be a template engine plugin!
            $declaredClassReflection = new ReflectionClass($declaredclass);
            if ($declaredClassReflection->implementsInterface('template_plugin'))
                 $this->pluginlist[] = $declaredclass;
        }
    }

    public function getImplementedFileFormats(){
        $outputlist = array();
        foreach($this->pluginlist as $engine){
            $outputlist = array_merge($engine::getImplementedFileFormats(), $outputlist);
        }
        return $outputlist;
    }

    public function getImplementedFunctions(){
        
    }
}

