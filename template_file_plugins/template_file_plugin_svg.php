<?php
/*
    Copyright 2011 Jacob Pieter van Wingerden

*/
include_once "template_file_plugin_interface.php";

/**
 * Description of template_file_plugin_svg
 *
 * @author Jaap
 */
class template_file_plugin_svg implements template_plugin {
    private $svgdata = null;

    private function unitToMillimeters($sizeString){
        // Inkscape seems to assume 3,543307086 pixels per mm, which
        // corresponds to a device resolution of 90 DPI. Since inkscape seems to
        // set viewport size in user units (px), and this web-to-print system
        // might need to output a fixed size unit (both rendering for screen and for
        // print might be requested), always assume one millimeter is 3,543307086
        // user units
        
        if (empty ($sizeString))
            throw new Exception("template_file_plugin_svg.php:unitToMillimeters() - No valid string passed for length parsing.");

        $matches = array();
        $preg_return = preg_match('/(?P<size>\\d{1,4}\\.+\\d*)\\s?(?P<unit>(px)?(mm)?(cm)?(in)?(pt)?(pc)?)/', $sizeString, $matches);

        if (0 == $preg_return)
            throw new Exception('template_file_plugin_svg.php:unitToMillimeters() - No valid size specification found in the string "'. $sizeString. '"');
        if (is_bool($preg_return))
            throw new Exception('template_file_plugin_svg.php:unitToMillimeters() - Regular expression function preg_match() returned an error.');
        if (!array_key_exists('size', $matches))
            throw new Exception('template_file_plugin_svg.php:unitToMillimeters() - Regular expression returned no valid size component.');
        if (!is_numeric($matches['size']))
            throw new Exception('template_file_plugin_svg.php:unitToMillimeters() - Size found seems to be invalid.');

        switch ($matches['unit']){
            case 'mm': {
                return $matches['size'];
            }
            case 'cm': {
                return ($matches['size'] * 10);
            }
            case 'in': {
                return ($matches['size'] * 25.4);
            }
            case 'pt': {
                return ($matches['size'] * 0.352777755);
            }
            case 'pc': {
                return ($matches['size'] * 4.233333316);
            }
            default: {
                return ($matches['size'] / 3.543307086);
            }
        }
        throw new Exception('template_file_plugin_svg.php:unitToMillimeters() - No value returned before this statement: this exception shouldn\'t have been called.');
    }

    private function isSVGLoaded(){
        if (is_null($this->svgdata))
            return false;
        return true;
    }

    /*
     * SVG data may contain links to external image files. However, we should make sure
     * those are not exploited by sending other data than images, and make sure
     * those images are actually there.
     */
    private function checkLinkedImages(){
        // Check if all linked image files are valid. If not, it might not be safe to use them.
        $listOfImages = $this->svgdata->xpath('//svg:image');
        foreach($listOfImages as $img){
            $imgatts = $img -> attributes('http://www.w3.org/1999/xlink');
            $toInlineFile = basename($imgatts['href']);
            if (file_exists($toInlineFile)){
                $theMimeType = mime_content_type($toInlineFile);
                if ((0 == strcmp($theMimeType, 'image/png')) ||
                    (0 == strcmp($theMimeType, 'image/jpeg')) ||
                    (0 == strcmp($theMimeType, 'image/gif')) ||
                    (0 == strcmp($theMimeType, 'image/tiff'))){
                        // $imgatts['href'] = 'data:'. $theMimeType. ';base64,'. base64_encode(file_get_contents($toInlineFile));
                        $imgatts['href'] = 'file:///'. $toInlineFile;

                } else {
                    // The file exists but is not a valid image. For safety reasons, it should not be linked to or included at all.
                    // Include a dummy file instead.
                    $imgatts['href'] = 'file:///dummy.jpg';
                    // Even although the linked image is not valid, don't bail out, just log it. The SVG data is still usable.
                    error_log('template_file_plugin_svg.php:getAvailableImageFields() - no valid image linked. Detected mimetype '. $theMimeType. ' for file '. $toInlineFile);
                }
            }
        }
    }

    private function setFieldValue($fieldName, $value){
        if (!$this->isSVGLoaded())
            throw new Exception('template_file_plugin_svg.php:setFieldValue() - No data loaded yet to get available text fields from.');
        
        if (!is_string($fieldName))
            throw new Exception('template_file_plugin_svg.php:setFieldValue() - passed Fieldname is not a string.');

        if (!is_string($value))
            throw new Exception('template_file_plugin_svg.php:setFieldValue() - passed value is not a string.');

        $FieldSearchResult = $this->svgdata->xpath('//svg:text[@id=\''. $fieldName .'\'] | //svg:image[@id=\''. $fieldName .'\'] | //svg:flowRoot[@id=\''. $fieldName .'\']');
        if (is_bool($FieldSearchResult))
            throw new Exception('template_file_plugin_svg.php:setFieldValue() - Field "'. $fieldName. '" not found.');

        foreach($FieldSearchResult as $result){
            if (0 == strcmp($result -> getName(), 'image')){
                foreach($result -> attributes('xlink', true) as $attribWaarde){ //
                        if (file_exists($value)){
                            $theMimeType = mime_content_type($value);
                            if ((0 == strcmp($theMimeType, 'image/png')) ||
                                (0 == strcmp($theMimeType, 'image/jpeg')) ||
                                (0 == strcmp($theMimeType, 'image/gif')) ||
                                (0 == strcmp($theMimeType, 'image/tiff'))){
                                    $result -> attributes('xlink', true) -> href = $value;
                                    return true;
                            } else {
                                return false;
                            }
                        } else {
                            // $result -> attributes('xlink', true) -> href = $value;
                            return false;
                        }
                }
            } else if (0 == strcmp($result -> getName(), 'text')){
                $textDataSearch = $result -> xpath('./svg:textPath[1] | ./svg:tspan[1]');
                foreach($textDataSearch as $fld){
                    $fld[0] = (string)$value;
                }
                return true;
            } else if (0 == strcmp($result->getName(), 'flowRoot')){
                // Delete any text this flowRoot element may contain.
                foreach($result->flowPara as $flowParaObject)
                    unset($flowParaObject);
                $flowrootCounter = 0;
                foreach(explode("\n", $value) as $line){
                    $line = str_replace("\r", '', $line);
                    $line = str_replace("\n", '', $line);
                    $addedFlowroot = $result->addChild('flowRoot', $line, 'http://www.w3.org/2000/svg');
                    $addedFlowroot->addAttribute('id', 'flowroot'. $flowrootCounter);
                    $flowrootCounter++;
                }

            }
        }
    }

    private function getFieldValue($fieldName){
        if (!$this->isSVGLoaded())
            throw new Exception('template_file_plugin_svg.php:getFieldValue() - No data loaded yet to set a field value on.');

        if (!is_string($fieldName))
            throw new Exception('template_file_plugin_svg.php:getFieldValue() - passed Fieldname is not a string.');

        $xpathSearch = '//svg:text[@id=\''. $fieldName .'\'] | //svg:image[@id=\''. $fieldName .'\']';
        $FieldSearchResult = $this->svgdata->xpath($xpathSearch);
        if (is_bool($FieldSearchResult))
            throw new Exception('template_file_plugin_svg.php:getFieldValue() - Field "'. $fieldName. '" not found.');

        foreach($FieldSearchResult as $result){
            if (0 == strcmp($result -> getName(), 'image')){
                foreach($result -> attributes('http://www.w3.org/1999/xlink') as $attribName => $attribValue){
                    if (0 == strcmp($attribName, 'href')){ // If attribute found:
                        return $attribValue;
                    }
                }
            } else if (0 == strcmp($result -> getName(), 'text')){
                $textDataSearch = $result -> xpath('./svg:textPath[1] | ./svg:tspan[1]');
                foreach($textDataSearch as $fld => $val){
                    return $val;
                }
            } else if (0 == strcmp($result->getName(), 'flowRoot')){
                $returnValue = '';
                foreach($result->flowPara as $flowParaObject){
                    $returnValue .= $flowParaObject[0]. "\n";
                }
                return $returnValue;
            }
        }
        throw new Exception('template_file_plugin_svg.php:getFieldValue() - No field found.');
        return false;
    }

    /*
     * Function getImplementedFileFormats() should return an array containing
     * the extensions of all file formats it can handle.
     */
    public static function getImplementedFileFormats(){
        return
            array(
                "svg"
            );
    }

    /*
     * function openString() should load a template from String. Text-based
     * formats, like SVG, can be passed directly. Binary formats should be
     * base64 encoded.
     */
    public function openString($templateData){
        if (!is_string($templateData))
            throw new Exception("template_file_plugin_svg.php:openString() - Given variable is not a string.");
        $this->svgdata->simplexml_load_string($templateData);
        $this->checkLinkedImages();
    }

    /*
     * Function openFile() should load a template file.
     * Takes one argument:
     *      $filename: string containing the path to the file that should be loaded.
     */
    public function openFile($filename){
        // Do checks: are we opening a valid supported file?
        if (!is_string($filename))
            throw new Exception("template_file_plugin_svg.php:openFile() - Given filename is not a string, and thus not a valid filename.");
        if (!file_exists($filename))
            throw new Exception("template_file_plugin_svg.php:openFile() - Given file does not exist or is not accessible.");
        if (!in_array(strtolower(pathinfo($filename, PATHINFO_EXTENSION)), $this->getImplementedFileFormats()))
            throw new Exception("template_file_plugin_svg.php:openFile() - Given file is not loadable by this plugin");

        // Load XML data
        $this->svgdata = simplexml_load_file($filename);
        if (is_bool($this->svgdata)){
            $this->svgdata = null; // Null indicates not loaded.
            throw new Exception("template_file_plugin_svg.php:openFile() - Could not open file ". $filename. ". Unknown error");
        }
        $this->checkLinkedImages();
    }

    /*
     * Function getAvailableTextFields should return an array containing a list
     * of the names textfields in in the template file which can be set by the user.
     */
    public function getAvailableTextFields(){
        if (!$this->isSVGLoaded())
            throw new Exception('template_file_plugin_svg.php:getAvailableTextFields() - No data loaded yet to get available text fields from.');
        $TextFields = $this->svgdata->xpath('//svg:text[@id]');
        $unknownfields = 0;
        $returnFields = array();
        if (is_array($TextFields)) foreach($TextFields as $tf){
            $currentFieldName = false;
            foreach($tf -> attributes() as $field => $value){
                if (0 == strcmp(''. $field, 'id')){
                    $currentFieldName = (string)$value;
                }
            }
            if (is_bool($currentFieldName)){
                $currentFieldName = 'field'. $unknownfields;
                error_log('Warning 2 in dsweb2print_templateengine.php in function getAvailableFields: No ID found for field in template '. $this -> thisTemplateName .'. Using auto-generated ID '. $currentFieldName);
                $unknownfields++;
            }
            $returnFields[] = $currentFieldName;
        }
        return $returnFields;
    }

    /*
     * Function getAvailableImageFields() should return an array containing a
     * list of the names of images in the template file which can be set by the user.
     */
    public function getAvailableImageFields(){
        if (!$this->isSVGLoaded())
            throw new Exception('template_file_plugin_svg.php:getAvailableTextFields() - No data loaded yet to get available text fields from.');
        $TextFields = $this->svgdata->xpath('//svg:image[@id]');
        $unknownfields = 0;
        $returnFields = array();
        if (is_array($TextFields)) foreach($TextFields as $tf){
            $currentFieldName = false;
            foreach($tf -> attributes() as $field => $value){
                if (0 == strcmp(''. $field, 'id')){
                    $currentFieldName = (string)$value;
                }
            }
            if (is_bool($currentFieldName)){
                $currentFieldName = 'field'. $unknownfields;
                error_log('Warning 2 in dsweb2print_templateengine.php in function getAvailableFields: No ID found for field in template '. $this -> thisTemplateName .'. Using auto-generated ID '. $currentFieldName);
                $unknownfields++;
            }
            $returnFields[] = $currentFieldName;
        }
        return $returnFields;
    }

    /*
     * Function setTextField() should set the contents of a text-field.
     * Takes two arguments:
     *      $textFieldName: a string with the name of the text-field to set, as returned by getAvailableTextFields()
     *      $textFieldValue: a string containing the new value of the tekstfield.
     */
    public function setTextField($textFieldName, $textFieldValue){
        $this->setFieldValue($textFieldName, $textFieldValue);
    }

    /*
     * Function setImageField() should set the contents of a image-field.
     * Takes two arguments:
     *      $imageFieldName: a string with the name of the image-field to set, as returned by getAvailableImageFields()
     *      $imageFieldValue: a string containing a path to an image file.
     */
    public function setImageField($imageFieldName, $imageFieldValue){
        $this->setFieldValue($imageFieldName, $imageFieldValue);
    }

    /*
     * Returns the contents of the text field specified in $fieldname (string)
     */
    public function getTextField($fieldname){
        return $this->getFieldValue($fieldname);
    }

    /*
     * Returns the path to the image specified in the imagefield specified in $fieldname (string)
     */
    public function getImageField($fieldname){
        return $this->getFieldValue($fieldname);
    }

    /*
     * Returns the template width in millimeters
     */
    public function getWidth(){
        if (!$this->isSVGLoaded())
             throw new Exception('template_file_plugin_svg.php:getWidth() - Cannot get width without any data loaded.');

        $viewportWidth = $this->svgdata->xpath('//svg:svg[@width]');
        if ((is_bool($viewportWidth)) || (0 == count($viewportWidth)))
             throw new Exception('template_file_plugin_svg.php:getWidth() - The viewport width has not been specified.');

        foreach ($viewportWidth as $simplexmlobj){
            foreach($simplexmlobj->attributes() as $attributeName => $attributeValue){
                $sizeunits['centimeter']    = 35.43307086614173;
                $sizeunits['millimeter']    = 3.543307086614173;
                $sizeunits['pixels']        = 1;
                $sizeunits['inch']          = 90;
                $sizeunits['point']         = 1.25;
                $sizeunits['pc']            = 15;

                if (0 == strcmp(strtolower($attributeName), 'width')){
                    try {
                        $millimeterunits = $this->unitToMillimeters($attributeValue);
                    } catch (Exception $e) {
                        throw new Exception('template_file_plugin_svg.php:getWidth() - Got the width string "'. $attributeValue. '", but according to unitToMillimeters() that\'s not a valid size!. It said: '. $e->getMessage());
                    }
                    return $millimeterunits;
                }
            }
        }
        throw new Exception('template_file_plugin_svg.php:getWidth() - The viewport attribute was found, but something got in the way reading it...');
    }

    /*
     * Returns te template height in millimeters
     */
    public function getHeight(){
        if (!$this->isSVGLoaded())
             throw new Exception('template_file_plugin_svg.php:getHeight() - Cannot get height without any data loaded.');

        $viewportHeight = $this->svgdata->xpath('//svg:svg[@height]');
        if ((is_bool($viewportHeight)) || (0 == count($viewportHeight)))
             throw new Exception('template_file_plugin_svg.php:getHeight() - The viewport width has not been specified.');

        foreach ($viewportHeight as $simplexmlobj){
            foreach($simplexmlobj->attributes() as $attributeName => $attributeValue){
                $sizeunits['centimeter']    = 35.43307086614173;
                $sizeunits['millimeter']    = 3.543307086614173;
                $sizeunits['pixels']        = 1;
                $sizeunits['inch']          = 90;
                $sizeunits['point']         = 1.25;
                $sizeunits['pc']            = 15;

                if (0 == strcmp(strtolower($attributeName), 'height')){
                    try {
                        $millimeterunits = $this->unitToMillimeters($attributeValue);
                    } catch (Exception $e) {
                        throw new Exception('template_file_plugin_svg.php:getHeight() - Got the height string "'. $attributeValue. '", but according to unitToMillimeters() that\'s not a valid size!. It said: '. $e->getMessage());
                    }
                    return $millimeterunits;
                }
            }
        }
        throw new Exception('template_file_plugin_svg.php:getHeight() - The viewport attribute was found, but something got in the way reading it...');
    }

    /*
     * Function getImplementedFunctions returns an array of functions available
     * for this type of template file engine. This is useful, since a template
     * engine might nog implement all functionality.
     * Some functions are required, and they don't have to be (but may be) specified:
     *      openFile()
     *      getImplementedFileFormats()
     *      getAvailableTextFields()
     *      getAvailableImageFields()
     *      setTextField($textFieldName, $textFieldValue)
     *      setImageField($imageFieldName, $imageFieldValue)
     *      getWidth()
     *      getHeight()
     */
    public static function getImplementedFunctions(){
        return array(
            'getImplementedFileFormats',
            'openFile',
            'openString',
            'getAvailableTextFields',
            'getAvailableImageFields',
            'setTextField',
            'setImageField',
            'getWidth',
            'getHeight',
            'getData',
            'getImageField',
            'getTextField'
        );
    }

    /*
     * Function getData() should return the current template data, usually for
     * output to a renderer. If no filename is specified, should return the
     * data as a string, base64 encoded in case of a binary format. When a
     * filename is specified, output to that file.
     */
    public function getData($filename = null){
        if ($this->isSVGLoaded()){
            if (is_null($filename))
                return $this->svgdata->asXML();
            $writeresult = file_put_contents($filename, $this->svgdata->asXML());
            if (strlen($this->svgdata->asXML()) == $writeresult)
                return true;
            throw new Exception('template_file_plugin_svg.php:getData() - Could not write to file "'. $filename. '".');
        }
        throw new Exception('template_file_plugin_svg.php:getData() - Cannot return data that hasn\'t even been loaded yet...');
    }

    public function addTextField($name = false, $top = 0, $left = 0, $width = 0, $height = 0){
        throw new Exception('template_file_plugin_svg.php:addTextField() - Not implemented!');
    }

    public function addImageField($name = false, $top = 0, $left = 0, $width = 0, $height = 0){
        throw new Exception('template_file_plugin_svg.php:addImageField() - Not implemented!');
    }
}

