<?php
/*
    Copyright 2011 Jacob Pieter van Wingerden

*/

/**
 *
 * @author Jaap
 *
 * A template plugin enables a template to be made out of a specific filetype.
 * Any template as defined here is always just one page. The web to print engine may
 * combine one or more instances of one or more templates to form pages. It may
 * also implement some settings and limitations that cannot be specified using
 * the file format this template plugin uses, or even override some it can.
 */
interface template_plugin {
    /*
     * Function getImplementedFileFormats() should return an array containing
     * the extensions of all file formats it can handle.
     */
    public static function getImplementedFileFormats();

    /*
     * function openString() should load a template from String. Text-based
     * formats, like SVG, can be passed directly. Binary formats should be
     * base64 encoded.
     */
    public function openString($templateData);

    /*
     * Function openFile() should load a template file.
     * Takes one argument:
     *      $filename: string containing the path to the file that should be loaded.
     */
    public function openFile($filename);

    /*
     * Function getAvailableTextFields should return an array containing a list
     * of the names textfields in in the template file which can be set by the user.
     */
    public function getAvailableTextFields();

    /*
     * Function getAvailableImageFields() should return an array containing a
     * list of the names of images in the template file which can be set by the user.
     */
    public function getAvailableImageFields();

    /*
     * Function setTextField() should set the contents of a text-field.
     * Takes two arguments:
     *      $textFieldName: a string with the name of the text-field to set, as returned by getAvailableTextFields()
     *      $textFieldValue: a string containing the new value of the tekstfield.
     */
    public function setTextField($textFieldName, $textFieldValue);

    /*
     * Function setImageField() should set the contents of a image-field.
     * Takes two arguments:
     *      $imageFieldName: a string with the name of the image-field to set, as returned by getAvailableImageFields()
     *      $imageFieldValue: a string containing a path to an image file.
     */
    public function setImageField($imageFieldName, $imageFieldValue);

    /*
     * Returns the contents of the text field specified in $fieldname (string)
     */
    public function getTextField($fieldname);

    /*
     * Returns the path to the image specified in the imagefield specified in $fieldname (string)
     */
    public function getImageField($fieldname);

    /*
     * Returns the template width in millimeters
     */
    public function getWidth();

    /*
     * Returns te template height in millimeters
     */
    public function getHeight();

    /*
     * Function getImplementedFunctions returns an array of functions available
     * for this type of template file engine. This is useful, since a template
     * engine might nog implement all functionality.
     * Some functions are required, and they don't have to be (but may be) specified:
     *      openFile()
     *      getImplementedFileFormats()
     *      getAvailableTextFields()
     *      getAvailableImageFields()
     *      setTextField($textFieldName, $textFieldValue)
     *      setImageField($imageFieldName, $imageFieldValue)
     *      rendertoFile($renderResolution = 300, $preferredRenderFormat = "auto")
     *      getWidth()
     *      getHeight()
     */
    public static function getImplementedFunctions();

    /*
     * Add a textfield to the template file.
     * Takes five arguments:
     *      $name: Anything other than a string should automatically generate a
     *             name for the added field. Otherwise, the passed string should
     *             specify the desired name for the new field.
     *      $top: Offset from the top of the template in millimeters
     *      $left: Offset from the left side of the template in millimeters
     *      $width: Width of the tekst field in millimeters. If set to 0, should adapt to contents
     *      $height: Height of the tekst field in millimeters. If set to 0, should adapt to contents
     *
     * Return value:
     *      Returns the name of the added field on succes. Should throw an
     *      exception on failure.
     */
    public function addTextField($name = false, $top = 0, $left = 0, $width = 0, $height = 0);

    /*
     * Add a imagefield to the template file.
     * Takes five arguments:
     *      $name: Anything other than a string should automatically generate a
     *             name for the added field. Otherwise, the passed string should
     *             specify the desired name for the new field.
     *      $top: Offset from the top of the template in millimeters
     *      $left: Offset from the left side of the template in millimeters
     *      $width: Image width in millimeters. If set to 0, make a guess
     *      $height: Image height in millimeters. If set to 0, make a guess
     *
     * Return value:
     *      Returns the name of the added field on succes. Should throw an
     *      exception on failure.
     */
    public function addImageField($name = false, $top = 0, $left = 0, $width = 0, $height = 0);

    /*
     * Function getData() should return the current template data, usually for
     * output to a renderer. If no filename is specified, should return the
     * data as a string, base64 encoded in case of a binary format. When a
     * filename is specified, output to that file.
     */
    public function getData($filename = null);
}
?>
