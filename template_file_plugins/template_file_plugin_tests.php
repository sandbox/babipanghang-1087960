<?php
/*
    Copyright 2011 Jacob Pieter van Wingerden

*/


/* 
 * Testing code for template file plugins
 */

    function echoln($echodata){
        echo($echodata. "<BR>\n");
    }

    /*
     * First series of tests: SVG plugin
     */

     include_once 'template_file_plugin_svg.php';

     $totalErrorCount = 0;

     $supportedFormats = template_file_plugin_svg::getImplementedFileFormats();
     foreach($supportedFormats as $fformat)
         echoln('template_file_plugin_svg reports supporting file format '. $fformat);

     $supportedFunctions = template_file_plugin_svg::getImplementedFunctions();
     foreach($supportedFunctions as $sfunction)
         echoln('template_file_plugin_svg reports supporting the function '. $sfunction);

     $svgPluginInstance = new template_file_plugin_svg();

     $functiontests['addImageField'] = false;
     $functiontests['addTextField'] = false;
     $functiontests['getAvailableImageFields'] = false;
     $functiontests['getAvailableTextFields'] = false;
     $functiontests['getData'] = false;
     $functiontests['getHeight'] = false;
     $functiontests['getWidth'] = false;
     $functiontests['getImageField'] = false;
     $functiontests['addTextField'] = false;
     $functiontests['setImageField'] = false;
     $functiontests['setTextField'] = false;


     echoln('Trying to execute functions on a template before loading any should result in exceptions being raised:');
     try {
        $svgPluginInstance->addImageField();
     } catch (Exception $e){
         $functiontests['addImageField'] = true;
     }
     echoln('addImageField: '. ($functiontests['addImageField'] ? 'OK' : 'FAIL'));

     try {
        $svgPluginInstance->addTextField();
     } catch (Exception $e){
         $functiontests['addTextField'] = true;
     }
     echoln('addTextField: '. ($functiontests['addTextField'] ? 'OK' : 'FAIL'));

     try {
        $svgPluginInstance->getAvailableImageFields();
     } catch (Exception $e){
         $functiontests['getAvailableImageFields'] = true;
     }
     echoln('getAvailableImageFields: '. ($functiontests['getAvailableImageFields'] ? 'OK' : 'FAIL'));

     try {
        $svgPluginInstance->getAvailableTextFields();
     } catch (Exception $e){
         $functiontests['getAvailableTextFields'] = true;
     }
     echoln('getAvailableTextFields: '. ($functiontests['getAvailableTextFields'] ? 'OK' : 'FAIL'));

     try {
        $svgPluginInstance->getData();
     } catch (Exception $e){
         $functiontests['getData'] = true;
     }
     echoln('getData: '. ($functiontests['getData'] ? 'OK' : 'FAIL'));

     try {
        $svgPluginInstance->getHeight();
     } catch (Exception $e){
         $functiontests['getHeight'] = true;
     }
     echoln('getHeight: '. ($functiontests['getHeight'] ? 'OK' : 'FAIL'));

     try {
        $svgPluginInstance->getWidth();
     } catch (Exception $e){
         $functiontests['getWidth'] = true;
     }
     echoln('getWidth: '. ($functiontests['getWidth'] ? 'OK' : 'FAIL'));

     try {
        $svgPluginInstance->getImageField('image3660');
     } catch (Exception $e){
         $functiontests['getImageField'] = true;
     }
     echoln('getImageField: '. ($functiontests['getImageField'] ? 'OK' : 'FAIL'));

     try {
        $svgPluginInstance->addTextField();
     } catch (Exception $e){
         $functiontests['addTextField'] = true;
     }
     echoln('addTextField: '. ($functiontests['addTextField'] ? 'OK' : 'FAIL'));

     try {
        $svgPluginInstance->setImageField('image3660', 'testimage.png');
     } catch (Exception $e){
         $functiontests['setImageField'] = true;
     }
     echoln('setImageField: '. ($functiontests['setImageField'] ? 'OK' : 'FAIL'));

     try {        
        $svgPluginInstance->setTextField('Bedrijfsnaam', "Hello, World!");
     } catch (Exception $e){
         $functiontests['setTextField'] = true;
     }
     echoln('setTextField: '. ($functiontests['setTextField'] ? 'OK' : 'FAIL'));

     foreach($functiontests as $testresult){
         if (!$testresult)
             $totalErrorCount++;
     }

     echoln('');
     echoln('Now trying to load testdata.');
     try {
         $svgPluginInstance->openFile('tests_data/sunny.svg');
     } catch(Exception $e){
        echoln('That failed with error message:');
        echoln($e->getMessage());
        echoln('Quitting tests.');
        $totalErrorCount++;
        echoln('Not all test ran succesfully. A total of '. $totalErrorCount. ' errors was encountered.');
        return;
     }
     echoln('Testdata loaded succesfully.');
     echoln('');
     echoln('Function test - getAvailableImageFields:');
     $functionerror = false;
     $availableImageFieldsList = array();
     try {
         $availableImageFieldsList = $svgPluginInstance->getAvailableImageFields();
     } catch (Exception $e){
         echoln('   FAILED with error message: '. $e->getMessage());
         $functionerror = true;
         $totalErrorCount++;
     }
     if (!$functionerror){
         echoln('Returned this list of available image fields:');
         foreach($availableImageFieldsList as $if)
             echoln('   - '. $if);
         echoln('');
         echoln('SUCCESS');
     }

     $functionerror = false;

     echoln('');
     echoln('Function test - getAvailableTextFields():');
     $availableTextFieldsList = array();
     try {
         $availableTextFieldsList = $svgPluginInstance->getAvailableTextFields();
     } catch(Exception $e){
         echoln('   FAILED with error message: '. $e->getMessage());
         $functionerror = true;
         $totalErrorCount++;
     }
     if (!$functionerror){
         echoln('Returned this list of available text fields:');
         foreach($availableTextFieldsList as $tf)
             echoln('   - '. $tf);
         echoln('');
         echoln('SUCCESS');
     }
     $functionerror = false;

     echoln('');
     echoln('Function test - getData():');
     try {
         $svgPluginInstance->getData();
     } catch (Exception $e){
         echoln('FAILED with error message: '. $e->getMessage());
         $functionerror = true;
         $totalErrorCount++;
     }
     if (!$functionerror){
         echoln('SUCCESS');
     }

     $functionerror = false;

     echoln('');
     echoln('Function test - getHeight():');
     $height = false;
     try {
         $height = $svgPluginInstance->getHeight();
     } catch (Exception $e) {
         echoln('FAILED with error message: '. $e->getMessage());
         $functionerror = true;
         $totalErrorCount++;
     }
     if (!$functionerror){
         echoln('Returned a height of '. $height. ' mm');
         echoln('SUCCESS');
     }
     
     $functionerror = false;

     echoln('');
     echoln('Function test - getWidth():');
     $width = false;
     try {
         $width = $svgPluginInstance->getWidth();
     } catch (Exception $e) {
         echoln('FAILED with error message: '. $e->getMessage());
         $functionerror = true;
         $totalErrorCount++;
     }
     if (!$functionerror){
         echoln('Returned a width of '. $width. ' mm');
         echoln('SUCCESS');
     }

     $functionerror = false;

     echoln('');
     echoln('Function test: getImageField():');
     echoln('Getting data on all image fields:');
     try {
         $imagelist = $svgPluginInstance->getAvailableImageFields();
         foreach($imagelist as $image){
             echoln($image. ': '. $svgPluginInstance->getImageField($image));
         }
     } catch (Exception $e) {
         echoln('FAILED with error message: '. $e->getMessage());
         $functionerror = true;
         $totalErrorCount++;
     }
     if (!$functionerror){
         echoln('Successfully returned the above list of values from the image fields.');
         echoln('SUCCESS');
     }

     $functionerror = false;
     echoln('');
     echoln('Function test: getTextField():');
     echoln('Getting data on all text fields:');
     try {
         $textslist = $svgPluginInstance->getAvailableTextFields();
         foreach($textslist as $mytext)
             echoln($mytext. ': '. $svgPluginInstance->getTextField($mytext));
     } catch (Exception $e) {
         echoln('FAILED with error message: '. $e->getMessage());
         $functionerror = true;
         $totalErrorCount++;
     }
     if (!$functionerror){
         echoln('Succesfully returned the above list of values from the text fields.');
         echoln('SUCCESS');
     }
     $functionerror = false;

     echoln('');
     echoln('Function test: setTextField():');
     try {
         $textlist = $svgPluginInstance->getAvailableTextFields();
         foreach($textlist as $xtext) {
             echoln('Value of field '. $xtext. ' was '. $svgPluginInstance->getTextField($xtext));
             echoln('Trying to set it to "foobar"');
             $svgPluginInstance->setTextField($xtext, 'foobar');
             echoln('Value of field '. $xtext. ' is now '. $svgPluginInstance->getTextField($xtext));
         }
     } catch (Exception $e) {
        echoln('FAILED with error message: '. $e->getMessage());
        $functionerror = true;
        $totalErrorCount++;
     }
     if (!$functionerror){
         echoln('Successfully changed all text fields');
         echoln('SUCCESS');
     }

     $functionerror = false;

     echoln('');
     echoln('Function test: setImageField():');
     try {
         $textlist = $svgPluginInstance->getAvailableImageFields();
         foreach($textlist as $xtext) {
             echoln('Value of field '. $xtext. ' was '. $svgPluginInstance->getImageField($xtext));
             echoln('Trying to set it to "foobar.jpg"');
             $svgPluginInstance->setImageField($xtext, 'foobar.jpg');
             echoln('Value of field '. $xtext. ' is now '. $svgPluginInstance->getImageField($xtext));
         }
     } catch (Exception $e) {
        echoln('FAILED with error message: '. $e->getMessage());
        $functionerror = true;
        $totalErrorCount++;
     }
     if (!$functionerror){
         echoln('Successfully changed all image fields');
         echoln('SUCCESS');
     }


     if ($totalErrorCount == 0)
         echoln('All tests ran succesfully.');
     else if ($totalErrorCount == 1)
         echoln('All tests completed. There was '. $totalErrorCount. ' error.');
     else
         echoln('All tests completed. There where '. $totalErrorCount. ' errors.');