<?php
    function find_inkscape_executable($default = 'inkscape'){
        $inkscape_paths[] = 'C:\\Program Files\\Inkscape\\inkscape.exe';
        $inkscape_paths[] = 'C:\\Program Files (x86)\\Inkscape\\inkscape.exe';
        $inkscape_paths[] = '/usr/bin/inkscape';
        foreach ($inkscape_paths as $ipath)
            if (file_exists($ipath)) return $ipath;
        return $default;
    }
