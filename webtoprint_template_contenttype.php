<?php

    // hook_form implementation
    // Disabled for now. Media module not in a usable state yet...
   /*
    function webtoprint_form($node, &$form_state) {
        $form = array();

        $form['webtoprint_templatefiles'] = array(
            '#type' => 'media',
            '#title' => t('Template files'),
            '#description' => t('Upload your template file(s) and all files linked to it.'),
            '#media_options' => array( // This doesn't work yet. Beta?
              'global' => array(
                'types' => array('other', 'video', 'audio', 'image'), // Example: array('image', 'audio');
              ),
              'multiselect' => TRUE,
            ),
        );
        return $form;
    }
    */

    /*
     * hook_form() implementation.
     * I hope that one day I'll be able to use the media-module or some other kind
     * of multi-file upload for this. For now, just limiting to a max of 5 files.
     */

    function webtoprint_numkeys($heystack, $search){
        $returncount = 0;
        foreach($heystack as $hey){
            $rval = stripos($hey, $search);
            $returncount += is_bool($rval) ? 0 : 1;
        }
        return $returncount;
    }

    function webtoprint_form($node, &$form_state) {
        $form = array();

        $form['uploaders'] = array(
            '#prefix' => '<div id="uploaders">',
            '#suffix' => '</div>',
        );

        
        $numFileFields = webtoprint_numkeys((array_key_exists('values', $form_state) ? array_keys($form_state['values']) : array()), 'webtoprint_templatefile');

        error_log(print_r($form_state, true));

        for ($a = 0;$a < (1 + $numFileFields); $a++){
            $form['uploaders']['webtoprint_templatefile'. $a] = array(
                '#type' => 'file',
                '#title' => t('Template files'),
                '#description' => t('Upload your template file(s) and all files linked to it.'),
            );
        }

        $form['uploaders']['updatebutton'] = array(
            '#type' => 'checkbox',
            '#value' => t('Add more files'),
            '#description' => t('Add more files.'),
            '#ajax' => array(
                'callback'  => 'webtoprint_template_contenttype_callback',
                'wrapper'   => 'uploaders',
                'event'     => 'change',
                'method'    => 'replace',
                'effect'    => 'fade',
            ),
        );
        
        return $form;
    }

    function webtoprint_template_contenttype_callback($form, $form_state){
        error_log('Callback fired!');
        return $form['uploaders'];
    }

    // hook_node_info() implementation
    function webtoprint_node_info() {
        $nodeinfo = array(
            'webtoprint_template' => array(
                'name' => t('Web to print template files'),
                'base' => 'webtoprint',
                'module' => 'templatefile',
                'description' => t("Add one or more web-to-print template files."),
                'help' => 'Upload files that together form a product template',
                'title_label' => t('Web to print template'),
                'has_body' => TRUE,
                'has_title' => TRUE,
            ),
        );
        $query_get_all_contenttypetest_nodes = "SELECT nid FROM {node} WHERE {node}.type = 'webtoprint_template'";
        return $nodeinfo;
    }

    function webtoprint_validate(){
        
    }

    function webtoprint_insert($node){
        
    }