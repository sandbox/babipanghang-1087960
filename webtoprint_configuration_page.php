<?php
    include_once('helperfunctions.php');

    function webtoprint_admin() {
      $form = array();

      $form['webtoprint_inkscapepath'] = array(
        '#type' => 'textfield',
        '#title' => t('Inkscape executable'),
        '#default_value' => variable_get('webtoprint_inkscapepath', find_inkscape_executable()),
        '#size' => 30,
        '#description' => t("Path to inkscape executable on the server."),
        '#required' => TRUE,
      );

      return system_settings_form($form);
    }

    function webtoprint_menu() {

      $items = array();

      $items['admin/settings/webtoprint'] = array(
        'page callback' => 'drupal_get_form',
        'page arguments' => array('webtoprint_admin'),
        'access arguments' => array('access administration pages'),
       );

      return $items;
    }

    function webtoprint_admin_validate($form, &$form_state) {
      $inkexe = $form_state['values']['webtoprint_inkscapepath'];
      if (!file_exists($inkexe))
          form_set_error('webtoprint_inkscapepath', t('No (executable) file was found at that location. Leave this field to "inkscape" to try a path search.'));
    }

